# Bedside book
[![made-with-latex](https://img.shields.io/badge/Made%20with-LaTeX-1f425f.svg?logo=latex)](https://www.latex-project.org/)

## Master branch
[![Gitpod Ready-to-Code](https://img.shields.io/badge/Gitpod-Ready--to--Code-blue?logo=gitpod)](https://gitpod.io/#prebuild/https://gitlab.com/conserto/explorers/bedside-book) 
[![pipeline status](https://gitlab.com/conserto/explorers/bedside-book/badges/master/pipeline.svg)](https://gitlab.com/conserto/explorers/bedside-book/-/commits/master)

## Develop branch
[![Gitpod Ready-to-Code](https://img.shields.io/badge/Gitpod-Ready--to--Code--on--Develop--branch-blue?logo=gitpod)](https://gitpod.io/#prebuild/https://gitlab.com/conserto/explorers/bedside-book/-/tree/develop) 
[![pipeline status](https://gitlab.com/conserto/explorers/bedside-book/badges/develop/pipeline.svg)](https://gitlab.com/conserto/explorers/bedside-book/-/commits/develop)

## Local quick Setup
Compile latex sources using docker:

### Make build script executable
```
wget https://raw.githubusercontent.com/blang/latex-docker/master/latexdockercmd.sh
chmod +x build.sh
```
### Compile using pdflatex (docker will pull the image automatically)
```
./build.sh pdflatex document.tex
```

### Or use latexmk (best option)
```
./build.sh latexmk -cd -f -interaction=batchmode -pdf document.tex
```

## Requirements
First, add your local user to docker group (should already be the case):
```
sudo usermod -aG docker YOURUSERNAME
```

The build.sh will use your current user and group id to compile.