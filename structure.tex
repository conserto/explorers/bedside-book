% ----  Required packages
\usepackage[utf8]{inputenc} % Required for inputting international characters
\usepackage[english, french]{babel}
\usepackage[babel=true]{csquotes}
\usepackage[T1]{fontenc} % Output font encoding for international characters
\usepackage{wrapfig}
\usepackage{float}
\usepackage[osf]{libertine} % Use the Libertine font
\usepackage{microtype} % Improves character and word spacing
\usepackage{lipsum}
\usepackage{pifont}
\usepackage{amssymb}
\usepackage{amsmath}
\usepackage{enumitem}
\usepackage{multicol}

\usepackage{tikz} % Required for drawing custom shapes
\definecolor{white}{RGB}{255,255,255}
\definecolor{blue}{RGB}{82,172,210}
\definecolor{pink}{RGB}{234,82,151}
\usepackage{wallpaper} % Required for setting background images (title page)
\usepackage{graphicx,eso-pic}

\newcommand\chapterimage[2][]{%
  \AddToShipoutPictureBG*{% Add picture to BackGround on this page only
    \AtTextUpperLeft{% Position at upper left of text block
      \hspace*{\textwidth}% Move over to upper right of text block
      \llap{% Ignore horizontal width and overlap to the left
        \smash{% Ignore vertical height
          \raisebox{-\height}{% Lower so top touches baseline
            \includegraphics[#1]{#2}}}}}}}% Include image with options

\newcommand{\checkeditem}{\item[\refstepcounter{enumi}$\text{\rlap{$\checkmark$}}\square$]}

\usepackage[
    pageanchor,
    unicode=true,
    bookmarks=true,
    bookmarksnumbered=true,
    bookmarksopen=false,
    breaklinks=false,
    pdfborder={0 0 1},
    backref=section,
    colorlinks=true,
    pdfpagemode=FullScreen,
    linkcolor = blue,
    anchorcolor = pink,
    citecolor = blue,
    filecolor = blue,
    urlcolor = blue]{hyperref} % PDF meta-information specification

% ---- Paper, margin and header/footer sizes

\setstocksize{12.5cm}{9.4cm} % Paper size
\settrimmedsize{\stockheight}{\stockwidth}{*} % No trims
\setlrmarginsandblock{18pt}{18pt}{*} % Left/right margins
\setulmarginsandblock{30pt}{36pt}{*} % Top/bottom margins
\setheadfoot{14pt}{12pt} % Header/footer height
\setheaderspaces{*}{8pt}{*} % Extra header space
\setlength{\parindent}{4em}
\setlength{\parskip}{1em}

% ---- Footnote customisation
\renewcommand{\foottextfont}{\itshape\footnotesize} % Font settings for footnotes
\setlength{\footmarkwidth}{-.1em} % Space between the footnote number and the text
\setlength{\footmarksep}{.1em} % Space between multiple footnotes on the same page
\renewcommand*{\footnoterule}{} % Remove the rule above the first footnote
\setlength{\skip\footins}{1\onelineskip} % Space between the body text and the footnote

% ---- Header and footer formats
\makepagestyle{mio} % Define a new custom page style
\setlength{\headwidth}{\textwidth} % Header the same width as the text
\makeheadrule{mio}{\textwidth}{0.1mm} % Header rule height
\makeoddhead{mio}{\scriptsize{\theauthor\hskip.2cm\vrule\hskip.2cm\itshape{\thetitle}}}{}{} % Header specification
\makeoddfoot{mio}{}{\scriptsize {\thepage \quad \vrule \quad \thelastpage}}{} % Footer specification
\makeoddfoot{plain}{}{\footnotesize {\thepage \quad \vrule \quad \thelastpage}}{} % Pages of chapters
\pagestyle{mio} % Set the page style to the custom style defined above

% ---- Part format
\renewcommand{\partnamefont}{\centering\sffamily\itshape\Large} % Part name font specification
\renewcommand{\partnumfont}{\sffamily\Large} % Part number font specification
\renewcommand{\parttitlefont}{\centering\sffamily\scshape} % Part title font specification
\renewcommand{\beforepartskip}{\null\vskip.618\textheight} % Whitespace above the part heading

% ---- Chapter format
\makechapterstyle{Tufte}{ % Define a new chapter style
\renewcommand{\chapterheadstart}{\null \vskip3.5\onelineskip} % Whitespace before the chapter starts
\renewcommand{\printchaptername}{\large\itshape\chaptername} % "Chapter" text font specification
\renewcommand{\printchapternum}{\LARGE\thechapter \\} % Chapter number font specification
\renewcommand{\afterchapternum}{} % Space between the chapter number and text
\renewcommand{\printchaptertitle}[1]{ % Chapter title font specification
\raggedright
\itshape\Large{##1}}
\renewcommand{\afterchaptertitle}{
\vskip3.5\onelineskip
}}
\chapterstyle{Tufte} % Set the chapter style to the custom style defined above

% ---- Section format
\setsecheadstyle{\sethangfrom{\noindent ##1}\raggedright\sffamily\itshape\Large} % Section title font specification
\setbeforesecskip{-.6\onelineskip} % Whitespace before the section
\setaftersecskip{.3\onelineskip} % Whitespace after the section

% ---- Subsection format
\setsubsecheadstyle{\sethangfrom{\noindent  ##1}\raggedright\sffamily\Large} % Subsection title font specification
\setbeforesubsecskip{-.5\onelineskip} % Whitespace before the subsection
\setaftersubsecskip{.2\onelineskip} % Whitespace after the subsection

% ---- Subsubsection format
\setsubsubsecheadstyle{\sethangfrom{\noindent ##1}\raggedright\sffamily\itshape\small} % Subsubsection title font specification
\setbeforesubsubsecskip{-.5\onelineskip} % Whitespace before the subsubsection
\setaftersubsubsecskip{.1\onelineskip} % Whitespace after the subsubsection

% ---- Caption format
\captiontitlefont{\itshape\footnotesize} % Caption font specification
\captionnamefont{\footnotesize} % "Caption" text font specification

% ---- Quotation environment format
\renewenvironment{quotation}
{\par\leftskip=1em\vskip.5\onelineskip\em}
{\par\vskip.5\onelineskip}

% ---- Quote environment format
\renewenvironment{quote}
{\list{}{\em\leftmargin=1em}\item[]}{\endlist\relax}

% ---- Miscellaneous document specification
\setlength{\parindent}{1em} % Paragraph indentation

%\midsloppy % Fewer overfull lines - used in the memoir class and allows a setting somewhere between \fussy and \sloppy

\checkandfixthelayout % Tell memoir to implement the above